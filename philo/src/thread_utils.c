/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   thread_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 11:01:07 by jrossign          #+#    #+#             */
/*   Updated: 2022/11/09 13:23:56 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	create_thread(t_data *data)
{
	int		i;
	t_philo	*philos;

	philos = data->philos;
	i = 0;
	while (i < data->nb_of_philo)
	{
		pthread_create(&philos->thread_id, NULL, start_sim, philos);
		pthread_detach(philos->thread_id);
		i++;
		philos = philos->next;
	}
}
