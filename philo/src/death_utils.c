/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   death_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/09 15:12:06 by jrossign          #+#    #+#             */
/*   Updated: 2022/11/10 12:44:43 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	check_for_death(t_data *data)
{
	int	done_eating_count;

	done_eating_count = 0;
	while (data->philos != NULL)
	{
		pthread_mutex_lock(&data->data_mutex);
		if (data->philos->meal_count >= data->nb_must_eat
			&& data->philos->done_eating == 0)
		{
				done_eating_count++;
				data->philos->done_eating = 1;
		}
		if (done_eating_count == data->nb_of_philo && data->nb_must_eat >= 0)
			set_end_status(data, DONE_EATING, 1);
		if ((get_time_utils() - data->philos->last_meal) > data->time_to_die)
			set_end_status(data, DIED, 2);
		if (data->simulation_done == 1)
			break ;
		pthread_mutex_unlock(&data->data_mutex);
		data->philos = data->philos->next;
		usleep(100);
	}
}

int	set_end_status(t_data *data, char *message, int flag)
{
	data->simulation_done = 1;
	pthread_mutex_unlock(&data->data_mutex);
	print_message(message, data->philos, flag);
	return (1);
}

int	check_dead_philo(t_data *data)
{
	pthread_mutex_lock(&data->data_mutex);
	if (data->simulation_done == 1)
	{
		pthread_mutex_unlock(&data->data_mutex);
		return (1);
	}
	pthread_mutex_unlock(&data->data_mutex);
	return (0);
}
