/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   simulation_utils.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 11:01:37 by jrossign          #+#    #+#             */
/*   Updated: 2022/11/10 15:56:18 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include <pthread.h>
#include <unistd.h>

void	try_eat(t_philo *philos)
{
	pthread_mutex_lock(&philos->fork);
	if (check_dead_philo(philos->data) == 0)
		print_message(TAKE_FORK, philos, 0);
	if (philos->data->nb_of_philo == 1)
		return ;
	pthread_mutex_lock(&philos->next->fork);
	if (check_dead_philo(philos->data) == 0)
		print_message(TAKE_FORK, philos, 0);
	if (check_dead_philo(philos->data) == 0)
		print_message(IS_EATING, philos, 0);
	pthread_mutex_lock(&philos->data->data_mutex);
	philos->last_meal = get_time_utils();
	pthread_mutex_unlock(&philos->data->data_mutex);
	sleeper(philos->data->time_to_eat);
	pthread_mutex_unlock(&philos->next->fork);
	pthread_mutex_unlock(&philos->fork);
	pthread_mutex_lock(&philos->data->data_mutex);
	philos->meal_count++;
	pthread_mutex_unlock(&philos->data->data_mutex);
	if (check_dead_philo(philos->data) == 0)
		try_sleep(philos);
}

void	try_think(t_philo *philos)
{
	print_message(IS_THINKING, philos, 0);
}

void	try_sleep(t_philo *philos)
{
	print_message(IS_SLEEPING, philos, 0);
	sleeper(philos->data->time_to_sleep);
}
