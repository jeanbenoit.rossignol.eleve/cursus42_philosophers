/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_data.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 11:01:17 by jrossign          #+#    #+#             */
/*   Updated: 2022/11/10 16:00:57 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include <pthread.h>

void	free_philo(t_data *data)
{
	pthread_mutex_lock(&data->philos->fork);
	pthread_mutex_unlock(&data->philos->fork);
	pthread_mutex_destroy(&data->philos->fork);
	pthread_mutex_lock(&data->data_mutex);
	free(data->philos);
	pthread_mutex_unlock(&data->data_mutex);
}

void	free_data(t_data *data)
{
	t_philo	*tmp;

	usleep(200000);
	if (data->nb_of_philo == 1)
	{
		free_philo(data);
		return ;
	}
	while (data->philos->id != 1)
		data->philos = data->philos->next;
	while (data->philos->is_last == 0)
	{
		tmp = data->philos->next;
		free_philo(data);
		data->philos = tmp;
	}
	tmp = data->philos->next;
	free_philo(data);
	pthread_mutex_destroy(&data->data_mutex);
	pthread_mutex_destroy(&data->msg);
}
