/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   routine.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 11:03:13 by jrossign          #+#    #+#             */
/*   Updated: 2022/11/10 15:57:04 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include <pthread.h>

void	*start_sim(void *philo)
{
	t_philo	*philos;

	philos = (t_philo *)philo;
	if (philos->id % 2 == 0)
		usleep(15000);
	while (1)
	{
		if (check_dead_philo(philos->data) == 1)
			break ;
		if (check_dead_philo(philos->data) == 0)
			try_eat(philos);
		if (philos->data->nb_of_philo == 1)
		{
			pthread_mutex_unlock(&philos->fork);
			break ;
		}
		if (check_dead_philo(philos->data) == 0)
			try_think(philos);
	}
	return (NULL);
}
