/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 11:00:47 by jrossign          #+#    #+#             */
/*   Updated: 2022/11/09 16:46:16 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	main(int argc, char **argv)
{
	t_data	data;
	int		return_value;

	if (argc == 5 || argc == 6)
	{
		return_value = set_data(&data, ++argv, argc);
		if (return_value > 0)
		{
			error_message(return_value);
			return (return_value);
		}
		create_thread(&data);
		check_for_death(&data);
		free_data(&data);
	}
	else
		error_message(PHILO_ARGC_C);
	return (EXIT_SUCCESS);
}
