/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_list_utils.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 11:04:48 by jrossign          #+#    #+#             */
/*   Updated: 2022/11/09 16:50:37 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

t_philo	*create_list(t_data *data)
{
	int		i;
	t_philo	*philos;

	i = 1;
	philos = NULL;
	while (i <= data->nb_of_philo)
	{
		add_philo(&philos, create_philo(i, data));
		i++;
	}
	data->philos = philos;
	return (philos);
}

t_philo	*create_philo(int id, t_data *data)
{
	t_philo	*philo;

	philo = malloc(sizeof(t_philo));
	if (!philo)
		return (NULL);
	philo->id = id;
	philo->is_last = 1;
	philo->meal_count = 0;
	philo->done_eating = 0;
	philo->last_meal = get_time_utils();
	pthread_mutex_init(&philo->fork, NULL);
	philo->next = NULL;
	philo->data = data;
	return (philo);
}

t_philo	*get_last(t_philo *philos)
{
	t_philo	*tmp;

	tmp = philos;
	if (!philos)
		return (NULL);
	while (tmp->is_last != 1)
		tmp = tmp->next;
	return (tmp);
}

void	add_philo(t_philo **philos, t_philo *new_philo)
{
	t_philo	*last_philo;
	t_philo	*first_philo;

	first_philo = *philos;
	if (!*philos)
	{
		new_philo->next = new_philo;
		*philos = new_philo;
	}
	else
	{
		last_philo = get_last(*philos);
		last_philo->is_last = 0;
		last_philo->next = new_philo;
		new_philo->next = first_philo;
	}
}
