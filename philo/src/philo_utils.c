/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 11:05:51 by jrossign          #+#    #+#             */
/*   Updated: 2022/11/03 14:25:32 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	ft_atoi(char *nb)
{
	int	i;
	int	result;

	i = 0;
	result = 0;
	while (nb[i])
	{
		if (nb[i] >= '0' && nb[i] <= '9')
			result = result * 10 + nb[i] - '0';
		else
			return (-1);
		i++;
	}
	return (result);
}

void	sleeper(int sleep_time)
{
	long long	time_end;
	long long	time_now;

	time_end = get_time_utils();
	while (1)
	{
		time_now = get_time_utils() - time_end;
		if (time_now >= sleep_time)
			break ;
		usleep(50);
	}
}

long long	get_time_utils(void)
{
	struct timeval	time;

	gettimeofday(&time, NULL);
	return ((time.tv_sec * 1000) + (time.tv_usec / 1000));
}
