/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_message.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 11:11:24 by jrossign          #+#    #+#             */
/*   Updated: 2022/11/09 15:37:39 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include <stdio.h>

void	print_message(char *mes, t_philo *philo, int flag)
{
	if (flag == 0)
	{
		pthread_mutex_lock(&philo->data->msg);
		printf("%llu %i %s\n", (get_time_utils() - philo->data->time_start),
			philo->id, mes);
		pthread_mutex_unlock(&philo->data->msg);
	}
	else if (flag == 1)
		flag_1(mes, philo);
	else if (flag == 2)
		flag_2(mes, philo);
}

void	flag_1(char *mes, t_philo *philo)
{
	pthread_mutex_lock(&philo->data->msg);
	if (philo->data->nb_must_eat > 1)
		printf("%llu %s %i times\n",
			(get_time_utils() - philo->data->time_start), \
			mes, philo->data->nb_must_eat);
	else
		printf("%llu %s %i time\n",
			(get_time_utils() - philo->data->time_start),
			mes, philo->data->nb_must_eat);
	pthread_mutex_unlock(&philo->data->msg);
}

void	flag_2(char *mes, t_philo *philo)
{
	pthread_mutex_lock(&philo->data->msg);
	printf("%llu %i %s\n", (get_time_utils() - philo->data->time_start),
		philo->id, mes);
	pthread_mutex_unlock(&philo->data->msg);
}

int	error_message(int message)
{
	if (message == PHILO_ZERO_C)
		printf("%s\n", PHILO_ZERO_M);
	if (message == PHILO_NOT_DIGIT_C)
		printf("%s\n", PHILO_NOT_DIGIT_M);
	if (message == PHILO_ARGC_C)
		printf("%s\n", PHILO_ARGC_M);
	if (message == MUTEX_ERROR_C)
		printf("%s\n", MUTEX_ERROR_M);
	return (message);
}
