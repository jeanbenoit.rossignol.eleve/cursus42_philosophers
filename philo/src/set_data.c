/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_data.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 11:03:56 by jrossign          #+#    #+#             */
/*   Updated: 2022/11/09 16:36:35 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"
#include <stdlib.h>

int	set_time(t_data *data, char **argv, int argc)
{
	data->nb_of_philo = ft_atoi(argv[0]);
	if (data->nb_of_philo <= 0)
		return (PHILO_ZERO_C);
	data->time_to_die = ft_atoi(argv[1]);
	data->time_to_eat = ft_atoi(argv[2]);
	data->time_to_sleep = ft_atoi(argv[3]);
	if (data->time_to_die == -1 || data->time_to_eat == -1
		|| data->time_to_sleep == -1)
		return (PHILO_NOT_DIGIT_C);
	data->nb_must_eat = -1;
	if (argc == 6)
	{
		data->nb_must_eat = ft_atoi(argv[4]);
		if (data->nb_must_eat == -1)
			return (PHILO_NOT_DIGIT_C);
	}
	return (EXIT_SUCCESS);
}

int	set_data(t_data *data, char **argv, int argc)
{
	int	return_val;

	return_val = set_time(data, argv, argc);
	if (return_val != EXIT_SUCCESS)
		return (return_val);
	data->simulation_done = 0;
	create_list(data);
	if (pthread_mutex_init(&data->msg, NULL) == -1)
		return (MUTEX_ERROR_C);
	if (pthread_mutex_init(&data->data_mutex, NULL) == -1)
		return (MUTEX_ERROR_C);
	data->time_start = get_time_utils();
	return (EXIT_SUCCESS);
}
