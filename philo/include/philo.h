/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/02 11:00:20 by jrossign          #+#    #+#             */
/*   Updated: 2022/11/10 15:59:17 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <pthread.h>
# include <sys/time.h>
# include <time.h>

# define TAKE_FORK "has taken a fork"
# define IS_EATING "is eating"
# define IS_THINKING "is thinking"
# define IS_SLEEPING "is sleeping"
# define DIED "died"
# define DONE_EATING "everyone has eaten at least"

//error_code
# define PHILO_ZERO_C 2
# define PHILO_NOT_DIGIT_C 3
# define PHILO_ARGC_C 4
# define MUTEX_ERROR_C 5
# define MALLOC_ERROR_C 6

//error_message
# define PHILO_ZERO_M "You need at least 1 philosopher to start the simulation!"
# define PHILO_NOT_DIGIT_M "The arguments need to be positives numbers \
and only digits!"
# define PHILO_ARGC_M "You must pass at least 4 arguments to the program: \
nb_of_philo, time_to_die, time_to_eat and time_to_sleep. You can add a \
fifth one the represent the number of times each philosophers should eat."
# define MUTEX_ERROR_M "An error occured when creating a mutex."
# define MALLOC_ERROR_M "An error occured when allocating memory."

typedef struct s_data
{
	int				nb_of_philo;
	int				time_to_die;
	int				time_to_eat;
	int				time_to_sleep;
	int				nb_must_eat;
	int				simulation_done;
	long long		time_start;
	pthread_mutex_t	msg;
	pthread_mutex_t	data_mutex;
	struct s_philo	*philos;
}					t_data;

typedef struct s_philo
{
	int				id;
	int				is_last;
	int				meal_count;
	int				done_eating;
	long long		last_meal;
	pthread_t		thread_id;
	pthread_mutex_t	fork;
	struct s_philo	*next;
	struct s_data	*data;
}					t_philo;

//death_utils.c
void		check_for_death(t_data *data);
int			set_end_status(t_data *data, char *message, int flag);
int			check_dead_philo(t_data *data);

//free_data.c
void		free_philo(t_data *data);
void		free_data(t_data *data);

//philo_list_utils.c
t_philo		*create_list(t_data *data);
t_philo		*create_philo(int id, t_data *data);
t_philo		*get_last(t_philo *philos);
void		add_philo(t_philo **philos, t_philo *new_philo);

//philo_utils.c
int			ft_atoi(char *nb);
void		sleeper(int sleep_time);
long long	get_time_utils(void);

//print_message.c
void		print_message(char *mes, t_philo *philo, int flag);
void		flag_1(char *mes, t_philo *philo);
void		flag_2(char *mes, t_philo *philo);
int			error_message(int message);

//routine.c
void		*start_sim(void *philo);

//set_data.c
int			set_time(t_data *data, char **argv, int argc);
int			set_data(t_data *data, char **argv, int argc);

//simulation_utils.c
void		try_eat(t_philo *philos);
void		try_think(t_philo *philos);
void		try_sleep(t_philo *philos);

//thread_utils.c
void		create_thread(t_data *data);

#endif
